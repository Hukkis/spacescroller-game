package com.hukkis.spaceshooter;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.brashmonkey.spriter.Player;
import com.hukkis.spaceshooter.States.GameStateManager;
import com.hukkis.spaceshooter.States.MenuState;

/**
 * The main class which initializes the game.
 * Spritebatch, GameStateManager and BitmapFont are created here.
 * This class calls the render method automatically each frame.
 * The render method calls update and render from GameStateManager,
 * which knows the current state to be drawn on screen.
 *
 */

public class SpaceShooter extends ApplicationAdapter {


	private GameStateManager gsm;

	SpriteBatch batch;
	Texture img;


	// Create the game, initialize spritebatch etc.

	@Override
	public void create () {

		batch = new SpriteBatch();
		Gdx.gl.glClearColor(0,0,0,1);

		gsm = new GameStateManager();

		//Push a MenuState to the stack and give GameStateManager to it.
		gsm.push(new MenuState(gsm));


	}

	// Render graphics to the screen.
	@Override
	public void render () {

		// Clear the screen for next set of images to be drawn.
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//Update and render logic is managed by the GameStateManager
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(batch);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
