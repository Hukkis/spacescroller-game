package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;


import java.util.Random;

/**
 * Created by Eetu on 6.3.2018.
 * Asteroid class.
 * Simple enemy type that moves between a random spawn point and
 * a random destination.
 */

public class Asteroid extends Enemy{

    Random randomSpawn;
    Random randomDestination;


    public Asteroid()
    {        
        sprite = new Sprite(new Texture(Gdx.files.internal("blueship.png")));

        speed = 80;

        // Random values for spawnPoint and destination.
        randomSpawn = new Random();
        randomDestination = new Random();


        // Vector defining the spawning position of the Asteroid.
        spawnPos = new Vector2(randomSpawn.nextInt(Gdx.graphics.getWidth() - 1) + 1, Gdx.graphics.getHeight() + sprite.getHeight());

        // Vector defining the destination point.
        destination = new Vector2(randomDestination.nextInt(Gdx.graphics.getWidth() - 1) + 1,0);

        // Direction of the Asteroid. Destination - spawnPosition vector normalized.
        direction = new Vector2(destination.sub(spawnPos).nor());

        // Set the current position to match the spawn position.
        setPosition(spawnPos.x,spawnPos.y);

        // Set bounds and a rectangle working as a collider.
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
        rectangle = new Rectangle(getX(),getY(),sprite.getWidth(),sprite.getHeight());
    }


    // Update the position towards the destination with direction vector.
    @Override
    public void update(float deltatime)
    {
        if(active)
        {
            setPosition(getX() + (direction.x * 2 * speed * deltatime), getY() + (direction.y * 2 * speed * deltatime));
            super.update(deltatime);
        }
    }

    // Reset the position of the asteroid with a new random value.
    @Override
    public void resetPosition()
    {
        // Set a new position, destination and direction.
        setPosition(randomSpawn.nextInt(Gdx.graphics.getWidth() - 1) + 1, Gdx.graphics.getHeight());
        destination.set(randomDestination.nextInt(Gdx.graphics.getWidth() - 1) + 1, - 50);
        spawnPos.set(getX(), getY());
        direction.set(destination.sub(spawnPos).nor());

        super.resetPosition();
    }

    @Override
    public void isDestroyed()
    {
        resetPosition();
        addAction(Actions.removeActor());
        setActive(false);
    }


}
