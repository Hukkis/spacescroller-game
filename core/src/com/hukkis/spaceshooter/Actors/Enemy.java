package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

/**
 * Created by Eetu on 7.3.2018.
 * Works as a baseclass for all enemies.
 * extends Actor so that all enemies can be added on stage.
 */

public class Enemy extends Actor{

    Rectangle rectangle;
    float spawnPosX;
    float spawnPosY;
    Vector2 spawnPos;
    Vector2 destination;
    Vector2 direction;
    boolean active = false;
    Sprite sprite;
    int speed;


    public Enemy()
    {

    }

    public void update(float deltatime)
    {
        if(active)
        {
            rectangle.setPosition(getX(),getY());
            positionChanged();
        }
    }

    public void takeDamage()
    {
        // to be added
    }

    public void isDestroyed()
    {

    }

    public void increaseSpeed(float addedSpeed)
    {
        speed += addedSpeed;
    }

    @Override
    protected void positionChanged()
    {
        super.positionChanged();
        sprite.setPosition(getX(),getY());
    }

    // Reset the position of the rectangle, so that it updates with the sprite.
    public void resetPosition()
    {
        rectangle.setPosition(getX(),getY());
    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {
        sprite.draw(batch);
    }

    public Rectangle getRectangle() { return rectangle; }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }



}
