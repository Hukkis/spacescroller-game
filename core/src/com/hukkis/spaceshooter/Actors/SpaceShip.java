package com.hukkis.spaceshooter.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Eetu on 5.3.2018.
 * Created so that this class is not dependent on anything else.
 */

public class SpaceShip extends Actor {

    private float speed;
    private int lives;
    private Sprite sprite = new Sprite(new Texture(Gdx.files.internal("spaceship.png")));
    private float invisibleTimer = 0;
    private boolean invinsible = false;

    // Works as a "collider".
    private Rectangle rectangle;


    public SpaceShip(int x, int y)
    {

        setPosition(x,y);
        setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
        rectangle = new Rectangle(getX(), getY(), sprite.getWidth(), sprite.getHeight());

        this.speed = 80;
        this.lives = 3;

    }

    // Updating the position of the spaceship via Accelerometer.
    public void update(float deltatime)
    {

        // Change the ship's position with accelerometer
        if(Gdx.input.getAccelerometerY() > 0.2)
            this.setPosition(getX() + (Gdx.input.getAccelerometerY() * 2 ) * speed * deltatime, getY());

        if(Gdx.input.getAccelerometerY() < 0.2)
            this.setPosition(getX() + ((Gdx.input.getAccelerometerY() *2) * speed * deltatime), getY());


        // To prevent the ship from going off screen
        if(getX() > Gdx.graphics.getWidth() - getWidth())
            setPosition(Gdx.graphics.getWidth() - getWidth(),getY());
        if(getX() < 0)
            setPosition(0,getY());


        positionChanged();

        // Move the rectangle with the sprite.
        rectangle.setPosition(getX(),getY());

        invisibleTimer -= deltatime;
        if(invisibleTimer < 0)
        {
            invisibleTimer = 0;
            invinsible = false;
        }
    }

    public void takeDamage()
    {
        if(invisibleTimer == 0)
        {
            invisibleTimer = 3.0f;
            invinsible = true;
            lives --;
        }
    }

    // To notify the actor that the position has changed
    @Override
    protected void positionChanged()
    {
        sprite.setPosition(getX(),getY());
        super.positionChanged();
    }

    @Override
    public void draw(Batch batch, float parentAlpha)
    {

        // If the ship is invinsible, change the color for the timer duration
        if(invisibleTimer != 0)
        {
            float changeColorTimer = (System.currentTimeMillis() % 300) / 300f;
            sprite.setColor(changeColorTimer,1- changeColorTimer, changeColorTimer, 1);
            sprite.draw(batch);
            sprite.setColor(1,1,1,parentAlpha);
        }
        else
        {
            sprite.draw(batch);
        }

    }

    public boolean getInvinsible() { return invinsible; }

    public int getLives() { return lives; }

    public Rectangle getRectangle() { return rectangle; }

}
