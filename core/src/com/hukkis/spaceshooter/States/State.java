package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Eetu on 18.2.2018.
 * Baseclass for states.
 * Defines a bunch of methods to be used in different States.
 *
 */

public abstract class State extends Stage {
    protected OrthographicCamera cam;
    protected Vector3 mouse;
    protected GameStateManager gsm;
    protected Stage stage;
    protected boolean Paused = false;

    // Constructor
    protected State (GameStateManager gsm){

        this.gsm = gsm;

        // Create a camera.
        cam = new OrthographicCamera();

        // Create a Stage to hold all the actors.
        stage = new Stage();

        // For testing purposes.
        mouse = new Vector3();

    }

    // For handling user input.
    protected abstract void handleInput();

    // For updating with deltatime. Sort of like Unity update.
    public abstract void update(float deltatime);

    // Defines what is rendered on the screen.
    public abstract void render(SpriteBatch spritebatch);


    // Get rid of the state
    public abstract void dispose();


    // Method for changing the state.
    // Works as a "scene loader".
    public void changeState(int index){

        switch (index)
        {
            case 0:
                gsm.set(new MenuState(gsm));
                break;

            case 1:
                gsm.set(new PlayState(gsm));
                break;

            default:
                break;
        }
    }

}
