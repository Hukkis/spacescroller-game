package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import javax.xml.soap.Text;

/**
 * Created by Eetu on 18.2.2018.
 *
 * A menu state that holds UI buttons.
 * Player can start or exit the game.
 */

public class MenuState extends State {


    private Table menuTable;

    private Skin playButtonSkin = new Skin(Gdx.files.internal("Data/glassy-ui.json"));
    private TextButton playButton;
    private TextButton exitButton;


    public MenuState(GameStateManager gsm){

        super(gsm);

        // Enable this stage to take user input.
        // Add actors.
        Gdx.input.setInputProcessor(stage);


        // Initialize textures, buttons etc.
        playButton = new TextButton("Play", playButtonSkin);
        exitButton = new TextButton("Exit", playButtonSkin);

        // Table to hold all the UI buttons.
        // Define table layout.
        menuTable = new Table();
        menuTable.setFillParent(true);
        menuTable.setWidth(1920);
        menuTable.setHeight(1080);

        menuTable.add(playButton).fillX().uniformX();
        menuTable.row().pad(10,0,10,0);
        menuTable.add(exitButton).fillX().uniformX();
        menuTable.row().pad(10,0,10,0);

        stage.addActor(menuTable);


        // Listeners for buttons.
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                changeState(1);
                dispose();
            }
        });

        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                Gdx.app.exit();
            }
        });
    }


    @Override
    public void render(SpriteBatch spritebatch) {

        spritebatch.begin();
        //spritebatch.draw(backGround, 0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.draw();
        spritebatch.end();
    }

    // Disposing of textures.
    @Override
    public void dispose()
    {
        playButtonSkin.dispose();
        stage.dispose();
        System.gc();
    }


    @Override
    public void handleInput() {}

    @Override
    public void update(float deltatime) {}

}
