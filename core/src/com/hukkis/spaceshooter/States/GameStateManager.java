package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;

import java.util.Stack;

/**
 * Created by Eetu on 18.2.2018.
 * Manages all the game states.
 * Game states work as "scenes" in game logic.
 * Stack structure.
 */

public class GameStateManager {

    private Stack<State> states;

    public GameStateManager(){
        states = new Stack<State>();
    }

    public void push(State state){
        states.push(state);
    }

    public void pop(){
        states.pop();
    }

    public void set(State state){
        states.pop();
        states.push(state);
    }

    // Peek the current state and update it's deltatime.
    public void update(float deltatime){
        states.peek().update(deltatime);
    }

    // Peek the current state and render on screen.
    // What is rendered is defined in each State.
    public void render(SpriteBatch spritebatch){
        states.peek().render(spritebatch);
    }
}
