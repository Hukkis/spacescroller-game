package com.hukkis.spaceshooter.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.hukkis.spaceshooter.Actors.Asteroid;
import com.hukkis.spaceshooter.Actors.Enemy;
import com.hukkis.spaceshooter.Actors.SpaceShip;
import com.hukkis.spaceshooter.Actors.Text;
import com.hukkis.spaceshooter.Effects.Explosion;
import com.hukkis.spaceshooter.Managers.BackgroundManager;
import com.sun.org.apache.xml.internal.utils.ObjectPool;

import java.awt.Label;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by Eetu on 28.2.2018.
 * "Main state" which contains the gameplay.
 */

public class PlayState extends State {

    // Variables for the pausemenu
    private Table pauseMenuTable;

    // Buttons.
    private Skin buttonSkin = new Skin(Gdx.files.internal("Data/glassy-ui.json"));
    private TextButton restartButton;
    private TextButton menuButton;
    private TextButton continueButton;
    private TextButton onGameMenuButton;

    // Score value and timer
    private int score = 0;
    private double scoreTimer = 5;

    // The player.
    private SpaceShip spaceShip;

    // Background with stars.
    private BackgroundManager backgroundManager;

    private BitmapFont font;

    // Explosion effect array.
    private ArrayList<Explosion> explosions = new ArrayList<Explosion>();

    private Texture lifeTexture;

    // Initialize an array of asteroids, so that they can be reused.
    private Enemy[] asteroids = new Enemy[] {
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
            new Asteroid(),
    };


    double spawnTimer = 0;
    double randomSpawnTimer;
    Random randomTime = new Random();


    // Create the game and run initializing methods.
    public PlayState(GameStateManager gsm) {
        super(gsm);

        Paused = false;

        spaceShip = new SpaceShip(600,100);
        stage.addActor(spaceShip);

        initializeMenu();
        initializeUI();
        spawnAsteroids();

    }


    // FUCKING MESSY, FIX IT!!!
    @Override
    public void update(float deltatime) {

        // Check if the game is paused. If it is, don't update.
        if(!Paused)
        {
            spaceShip.update(deltatime);

            // Update all asteroids and check if the spaceship collides with them.
            for (Enemy e : asteroids) {

                e.update(deltatime);
                checkCollisions(e);

                // If the asteroid is off screen destroy it
                if(e.getY() < -20)
                {
                    e.isDestroyed();
                }
            }


            if(spawnTimer >= randomSpawnTimer)
            {
                spawnAsteroid();
                spawnTimer = 0;
                randomSpawnTimer = 0.2 + (2-0.2) * randomTime.nextDouble();
            }

            if(scoreTimer >= 2)
            {
                score += 50;
                scoreTimer = 0;

            }


            spawnTimer += deltatime;
            scoreTimer += deltatime;
        }

        // Check if there are any explosions happening. If there are,
        // update their particles.
        if(explosions.size() > 0)
        {
            for(int i = 0; i < explosions.size(); ++i)
            {
                explosions.get(i).update(deltatime);

                if(explosions.get(i).timer <= 0)
                    explosions.remove(i);
            }
        }
    }

    @Override
    public void render(SpriteBatch spritebatch) {

        spritebatch.begin();
        backgroundManager.renderBackground(spritebatch);


        for(int i = 0; i < spaceShip.getLives(); ++i)
            spritebatch.draw(lifeTexture, lifeTexture.getWidth() * i + 15, Gdx.graphics.getHeight() - 75);

        font.draw(spritebatch, "Score: " + score, Gdx.graphics.getWidth() / 2,Gdx.graphics.getHeight() - 50);


        // Render explosions
        for(int i = 0; i < explosions.size(); ++i)
            explosions.get(i).renderParticles(spritebatch);


        spritebatch.end();
        stage.act();
        stage.draw();


    }

    // Check collisions between the spaceship and all enemies.
    private void checkCollisions(Enemy e)
    {
        if(spaceShip.getRectangle().overlaps(e.getRectangle()) && e.isActive() == true && spaceShip.getInvinsible() == false)
        {
            e.isDestroyed();
            spaceShip.takeDamage();
            explosions.add(new Explosion((spaceShip.getX() + spaceShip.getWidth() / 2), (spaceShip.getY() + spaceShip.getHeight() / 2)));

            if(spaceShip.getLives() == 0)
            {
                gameOver();
                Paused = true;
            }
            // make sound
        }
    }


    //Spawn all asteroids - for testing purposes
    private void spawnAsteroids()
    {
        for (Enemy a: asteroids)
        {
            stage.addActor(a);
            a.setActive(true);
        }
    }

    // Spawn a single asteroid, change this to work with multiple types.
    private void spawnAsteroid()
    {
        for (Enemy a : asteroids)
        {
            if(a.isActive() == false)
            {
                stage.addActor(a);
                a.setActive(true);
                a.resetPosition();
                break;
            }
        }
    }

    // Run when player dies.
    private void gameOver()
    {
        showDeathMenu();
    }

    @Override
    protected void handleInput() {
        // Empty for now
        // click to shoot?
    }

    // Create the background, fonts etc.
    private void initializeUI()
    {
        font = new BitmapFont();
        font.setColor(0,1,1,1);
        lifeTexture = new Texture("heart.png");
        backgroundManager = new BackgroundManager();

        onGameMenuButton = new TextButton("Menu", buttonSkin);
        //onGameMenuButton.setBounds(Gdx.graphics.getWidth() - 100, Gdx.graphics.getHeight() - 75, 50,50);
        onGameMenuButton.setSize(300,100);
        onGameMenuButton.setPosition(Gdx.graphics.getWidth() - onGameMenuButton.getWidth(), Gdx.graphics.getHeight() - onGameMenuButton.getHeight());

        onGameMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                showPauseMenu();
            }
        });

        stage.addActor(onGameMenuButton);
    }

    private void initializeMenu()
    {

        Gdx.input.setInputProcessor(stage);

        restartButton = new TextButton("Restart", buttonSkin);
        continueButton = new TextButton("Continue", buttonSkin);
        menuButton = new TextButton("Main Menu", buttonSkin);

        // Define the table layout.

        pauseMenuTable = new Table();
        pauseMenuTable.setFillParent(true);
        pauseMenuTable.setWidth(1920);
        pauseMenuTable.setHeight(1080);

        pauseMenuTable.add(continueButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);
        pauseMenuTable.add(restartButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);
        pauseMenuTable.add(menuButton).fillX().uniformX();
        pauseMenuTable.row().pad(10,0,10,0);


        // Listeners for buttons.
        restartButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                changeState(1);
                dispose();
            }
        });

        menuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                changeState(0);
            }
        });

        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Paused = false;
                pauseMenuTable.addAction(Actions.removeActor());
            }
        });

    }

    private void showPauseMenu()
    {
        Paused = true;
        stage.addActor(pauseMenuTable);
    }

    private void showDeathMenu()
    {
        Paused = true;
        pauseMenuTable.removeActor(pauseMenuTable.getChildren().first());
        stage.addActor(pauseMenuTable);
    }


    @Override
    public void dispose() {
        stage.dispose();
        backgroundManager.dispose();
        lifeTexture.dispose();
        buttonSkin.dispose();
        System.gc();
    }
}
