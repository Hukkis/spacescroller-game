package com.hukkis.spaceshooter.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Random;

/**
 * Created by Eetu on 8.3.2018.
 * Get background images and stars from a spritesheet.
 */

public class BackgroundManager {


    private Texture starSheet = new Texture("starsheet.png");
    public TextureRegion star1;
    public TextureRegion star2;
    public TextureRegion star3;

    public int starcount = 100;
    public float[] xPos;
    public float[] yPos;
    public int[] starType;
    public float[] brightness;
    public float[] starSpeed;



    public BackgroundManager()
    {

        // Get stars as a TextureRegion from the spritesheet.
        // Parameters are the position and size of the sprites.
        star1 = new TextureRegion(starSheet,0,16,32,32);
        star2 = new TextureRegion(starSheet,8,0,16,16);
        star3 = new TextureRegion(starSheet,0,0,8,8);

        xPos = new float[starcount];
        yPos = new float[starcount];
        starType = new int[starcount];
        brightness = new float[starcount];
        starSpeed = new float[starcount];

        // For randomizing the position and star type
        Random rand = new Random();
        for(int i = 0; i < starcount; ++i)
        {
            xPos[i] = rand.nextInt(Gdx.graphics.getWidth());
            yPos[i] = rand.nextInt(Gdx.graphics.getHeight());
            starType[i] = rand.nextInt(3);
            brightness[i] = rand.nextFloat();
            starSpeed[i] = rand.nextFloat()*3;
        }
    }



    // For rendering the background with moving stars
    // Texture region tr is looped for each star and it renders
    // a random star.
    public void renderBackground(SpriteBatch spriteBatch)
    {
        TextureRegion tr = null;


        for(int i = 0; i < starcount; ++i)
        {
            if (starType[i] == 0) tr = star1;
            if (starType[i] == 1) tr = star2;
            if (starType[i] == 2) tr = star3;


            // Change the alpha value of the spritebatch, in order to
            // render some of the stars as transparent
            spriteBatch.setColor(1.0f, 1.0f, 1.0f, brightness[i] * (0.2f + starType[i]) / 1.0f);
            spriteBatch.draw(tr,xPos[i], yPos[i], tr.getRegionWidth(), tr.getRegionHeight());

            yPos[i] = yPos[i] - starSpeed[i] *(0.5f + starType[i]);

            if(yPos[i] < -50)
                yPos[i] = yPos[i] + Gdx.graphics.getHeight() + 50;
        }

        // Change the transparency back.
        spriteBatch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public void dispose()
    {
        starSheet.dispose();
    }

}

























