package com.hukkis.spaceshooter.Effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Random;


/**
 * Created by Eetu on 8.3.2018.
 */

public class Explosion {

    private TextureRegion particle;
    private Texture starSheet = new Texture("starsheet.png");
    public float xPos[];
    public float yPos[];
    public float xDelta[];
    public float yDelta[];

    public float timer;
    int particleCount = 300;

    public Explosion(float x, float y)
    {
        xPos = new float[particleCount];
        yPos = new float[particleCount];
        xDelta = new float[particleCount];
        yDelta = new float[particleCount];

        particle = new TextureRegion(starSheet,0,0,8,8);
        Random rand = new Random();


        // Set the position to match the parameters and
        // randomize the movement direction of the particles
        for(int i = 0; i < xPos.length; ++i)
        {
            xPos[i] = x;
            yPos[i] = y;

            xDelta[i] = 0.5f - rand.nextFloat();
            yDelta[i] = 0.5f - rand.nextFloat();
        }

        timer = 2;

    }

    public void update(float deltatime)
    {
        timer = timer - deltatime;

        for(int i = 0; i < xPos.length; ++i)
        {
            xPos[i] += xDelta[i] * deltatime * 300;
            yPos[i] += yDelta[i] * deltatime * 300;
        }
    }

    public void renderParticles(SpriteBatch spriteBatch)
    {
        for(int i = 0; i < xPos.length; ++i)
        {
            spriteBatch.draw(particle, xPos[i], yPos[i]);
        }
    }



}
